Project #2:
Industry: Online Banking App
Project Description: Providing complete online banking solution to the customer
Project execution work:
1. Performing secure login and logout
2. Developing UI using JSP
3. Developing Servlets
4. Installing Oracle database
5. Connecting with database using hibernate and performing CRUD operations
6. Integrating with spring to remove dependency and for easy maintenance.
7. Performing credit / debit transactions
8. Checking Account Balance
9. Implementing Web services, WSDL files, SOAP, RESTFUL web service, JAX-RS implementationz
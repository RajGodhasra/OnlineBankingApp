<%--
  Created by IntelliJ IDEA.
  User: Raj Godhasara
  Date: 20-Jun-18
  Time: 11:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Online Banking Application</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" media="all"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/ajax-progress.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/autocomplete-loading.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/fieldgroup.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/container-inline.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/clearfix.module2d2c.css?pa3"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/details.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/hidden.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/item-list.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/js.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/nowrap.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/position-container.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/reset-appearance.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/resize.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/sticky-header.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-counter2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-report-counters2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-report-general-info2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tabledrag.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tablesort.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tree-child.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/animate2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/md-slider2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/md-slider-style2d2c.css?pa38zd"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/md-slider-css/md-slider-home_slider_1-layers2d2c.css?pa38zd"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/action-links2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/breadcrumb2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/button2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/collapse-processed2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/container-inline2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/details2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/exposed-filters2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/field2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/form2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/icons2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/inline-form2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/item-list2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/link2d2c.css?pa38zd   "/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/links2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/more-link2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/pager2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tabledrag2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tableselect2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tablesort2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tabs2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/textarea2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/ui-dialog2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/messages2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/style2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/responsive2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/update2d2c.css?pa38zd"/>


</head>
<body class="ltr path-frontpage home page-node-type-homepage home">

<a href="#main-content" class="visually-hidden focusable">
    Skip to main content
</a>
<div class="main-page-wrapper">
    <div id="inobi-map" class="hidden" data-lat="40.72" data-lang="-74"></div>
    <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
        <header class="theme-menu-wrapper">
            <div class="container">
                <div class="top-header ">
                    <div class="row">
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="call"><a href="#"><i class="flaticon-telephone"></i> +91 8866142423</a></div>
                        </div>
                        <div class="col-md-10 col-sm-3 col-xs-12">
                            <ul class="social-icon text-right">
                                <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header-wrapper clearfix">
                    <div id="block-inobi-branding" class="logo float-left">
                        <a href="<%=request.getContextPath() %>/index"><img
                                src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/logo.png"
                                alt="Home"></a>
                    </div>

                    <nav class="theme-main-menu navbar float-right" id="mega-menu-wrapper">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="navbar-collapse-1" class="collapse navbar-collapse">
                            <ul class="nav">
                                <li class="dropdown-holder active">
                                    <a href="<%=request.getContextPath() %>/index" class="tran3s is-active"
                                       data-drupal-link-system-path="&lt;front&gt;">Home</a>
                                    <%--<ul class="sub-menu">
                                        <li class="active">
                                            <a href="home1.html" class="tran3s is-active" data-drupal-link-system-path="node/37">Home version one</a>
                                        </li>
                                        <li>
                                            <a href="home2.html" class="tran3s" data-drupal-link-system-path="node/38">Home version two</a>
                                        </li>
                                    </ul>--%>
                                </li>
                                <%--<li class="dropdown-holder">
                                    <a href="#" class="tran3s is-active" data-drupal-link-system-path="&lt;front&gt;">Services</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="services-0.html" class="tran3s" data-drupal-link-system-path="node/40">Service</a>
                                        </li>
                                        <li>
                                            <a href="consulting-idea.html" class="tran3s" data-drupal-link-system-path="node/33">Services details</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder">
                                    <a href="#" class="tran3s is-active" data-drupal-link-system-path="&lt;front&gt;">Pages</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="404-page.html" class="tran3s" data-drupal-link-system-path="node/39">404 Page</a>
                                        </li>
                                        <li>
                                            <a href="about-us.html" class="tran3s" data-drupal-link-system-path="node/42">About us</a>
                                        </li>
                                        <li>
                                            <a href="node/46.html" class="tran3s" data-drupal-link-system-path="node/46">Faq</a>
                                        </li>
                                        <li>
                                            <a href="our-team.html" class="tran3s" data-drupal-link-system-path="node/43">Our Team</a>
                                        </li>
                                        <li class="dropdown-holder">
                                            <a href="shop.html" class="tran3s" data-drupal-link-system-path="shop">Shop</a>
                                            <ul class="second-sub-menu">
                                                <li>
                                                    <a href="long-tail-seo-strategy.html" class="tran3s" data-drupal-link-system-path="product/2">Shop Details</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder">
                                    <a href="#" class="tran3s" data-drupal-link-system-path="node/41">Porfolio</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="project.html" class="tran3s" data-drupal-link-system-path="node/41">Project</a>
                                        </li>
                                        <li>
                                            <a href="chanze-renovation-inc.html" class="tran3s" data-drupal-link-system-path="node/13">Project detail</a>
                                        </li>
                                    </ul>
                                </li>--%>
                                <li class="dropdown-holder">
                                    <a href="<%=request.getContextPath() %>/login" class="tran3s"
                                       data-drupal-link-system-path="blog">Login</a>
                                </li>
                                <li>
                                    <a href="#" class="tran3s" data-drupal-link-system-path="contact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <div id="block-homeslider1">
            <div id="md-slider-1-block" class="md-slide-items" style="min-height:680px" data-thumb-height="100"
                 data-thumb-width="100">
                <div class="wrap-loader-slider animated">
                    <div class="wrap-cube">
                        <div class="sk-cube-grid">
                            <div class="sk-cube sk-cube1"></div>
                            <div class="sk-cube sk-cube2"></div>
                            <div class="sk-cube sk-cube3"></div>
                            <div class="sk-cube sk-cube4"></div>
                            <div class="sk-cube sk-cube5"></div>
                            <div class="sk-cube sk-cube6"></div>
                            <div class="sk-cube sk-cube7"></div>
                            <div class="sk-cube sk-cube8"></div>
                            <div class="sk-cube sk-cube9"></div>
                        </div>
                    </div>
                </div>
                <div class="md-slide-item md-layer-1-1" data-timeout="8000"
                     data-transition="strip-up-right,strip-right-down,top-curtain"
                     data-thumb="http://inobiz.drupalet.com/sites/default/files/styles/thumbnail/public/md-slider-image/slide-1_0.jpg?itok=KNH1rwXX"
                     data-thumb-type="image" data-thumb-alt="">
                    <div class="md-main-img" style="">
                        <img src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image1349x680/public/md-slider-image/slide-1_05beb.jpg?itok=LKk9-w6f"
                             alt="slider"/>
                    </div>
                    <div class="md-slider-overlay" style="background: rgba(4, 3, 3, 0.5)">
                    </div>
                    <div class="md-objects">
                        <div class="md-object md-layer-1-1-0" data-x="103" data-y="205" data-width="238"
                             data-height="24" data-start="300" data-stop="8000" data-easein="fadeInUp">
                            Inpire Your Business
                        </div>
                        <div class="md-object md-layer-1-1-1" data-x="100" data-y="253" data-width="730"
                             data-height="50" data-start="700" data-stop="8000" data-easein="fadeInUp">
                            START CREATE YOUR
                        </div>
                        <div class="md-object md-layer-1-1-2" data-x="102" data-y="324" data-width="447"
                             data-height="50" data-start="1100" data-stop="8000" data-easein="fadeInUp">
                            B2B BUSINESS TOGETHER
                        </div>
                        <div class="md-object md-layer-1-1-3" data-x="103" data-y="416" data-width="100"
                             data-height="20" data-start="1600" data-stop="8000" data-easein="fadeInLeft"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Get More
                            </a>
                        </div>
                        <div class="md-object md-layer-1-1-4" data-x="295" data-y="416" data-width="100"
                             data-height="18" data-start="1600" data-stop="8000" data-easein="fadeInRight"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Contact Us
                            </a>
                        </div>
                    </div>
                </div>
                <div class="md-slide-item md-layer-1-2" data-timeout="8000"
                     data-transition="strip-left-up,strip-right-left-up,strip-up-down-right"
                     data-thumb="http://inobiz.drupalet.com/sites/default/files/styles/thumbnail/public/md-slider-image/slide-2.jpg?itok=O9PdR8Ld"
                     data-thumb-type="image" data-thumb-alt="">
                    <div class="md-main-img" style="">
                        <img src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image1349x680/public/md-slider-image/slide-23a07.jpg?itok=pjUaj-Y3"
                             alt="slider"/>
                    </div>
                    <div class="md-slider-overlay" style="background: rgba(4, 3, 3, 0.5)">
                    </div>
                    <div class="md-objects">
                        <div class="md-object md-layer-1-2-0" data-x="103" data-y="205" data-width="238"
                             data-height="24" data-start="300" data-stop="8000" data-easein="fadeInUp">
                            Inpire Your Business
                        </div>
                        <div class="md-object md-layer-1-2-1" data-x="100" data-y="253" data-width="730"
                             data-height="50" data-start="700" data-stop="8000" data-easein="fadeInUp">
                            START CREATE YOUR
                        </div>
                        <div class="md-object md-layer-1-2-2" data-x="102" data-y="324" data-width="447"
                             data-height="50" data-start="1100" data-stop="8000" data-easein="fadeInUp">
                            B2B BUSINESS TOGETHER
                        </div>
                        <div class="md-object md-layer-1-2-3" data-x="103" data-y="416" data-width="100"
                             data-height="20" data-start="1600" data-stop="8000" data-easein="fadeInLeft"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Get More
                            </a>
                        </div>
                        <div class="md-object md-layer-1-2-4" data-x="295" data-y="416" data-width="100"
                             data-height="18" data-start="1600" data-stop="8000" data-easein="fadeInRight"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Contact Us
                            </a>
                        </div>
                    </div>
                </div>
                <div class="md-slide-item md-layer-1-3" data-timeout="8000"
                     data-transition="strip-left-up,strip-right-left-up,strip-up-down-right"
                     data-thumb="http://inobiz.drupalet.com/sites/default/files/styles/thumbnail/public/md-slider-image/slide-3.jpg?itok=UQjK67cB"
                     data-thumb-type="image" data-thumb-alt="">
                    <div class="md-main-img" style="">
                        <img src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image1349x680/public/md-slider-image/slide-3534a.jpg?itok=A2DhZOdj"
                             alt="slider"/>
                    </div>
                    <div class="md-slider-overlay" style="background: rgba(4, 3, 3, 0.5)">
                    </div>
                    <div class="md-objects">
                        <div class="md-object md-layer-1-3-0" data-x="103" data-y="205" data-width="238"
                             data-height="24" data-start="300" data-stop="8000" data-easein="fadeInUp">
                            Inpire Your Business
                        </div>
                        <div class="md-object md-layer-1-3-1" data-x="100" data-y="253" data-width="730"
                             data-height="50" data-start="700" data-stop="8000" data-easein="fadeInUp">
                            START CREATE YOUR
                        </div>
                        <div class="md-object md-layer-1-3-2" data-x="102" data-y="324" data-width="447"
                             data-height="50" data-start="1100" data-stop="8000" data-easein="fadeInUp">
                            B2B BUSINESS TOGETHER
                        </div>
                        <div class="md-object md-layer-1-3-3" data-x="103" data-y="415" data-width="100"
                             data-height="20" data-start="1600" data-stop="8000" data-easein="fadeInLeft"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Get More
                            </a>
                        </div>
                        <div class="md-object md-layer-1-3-4" data-x="295" data-y="415" data-width="100"
                             data-height="18" data-start="1600" data-stop="8000" data-easein="fadeInRight"
                             data-paddingtop="16" data-paddingright="35" data-paddingbottom="16" data-paddingleft="35">
                            <a href="#">
                                Contact Us
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block-inobi-content">
            <article data-history-node-id="37" role="article" about="/home1" class="article">
                <div>
                </div>
            </article>
        </div>
        <div id="block-inobiblockourservice" class="our-service section-margin-bottom-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-service">
                            <div class="text">
                                <h4><a href="#">Top Services</a> <span>01</span></h4>
                                <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita fusce
                                    temp oribus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-service">
                            <div class="text">
                                <h4><a href="#">Business Consulting</a> <span>02</span></h4>
                                <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita fusce
                                    temp oribus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-service">
                            <div class="text">
                                <h4><a href="#">Expert Team Mates</a> <span>03</span></h4>
                                <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita fusce
                                    temp oribus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block-inobiblockconsultantbanner" class="consultant-banner section-margin-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <h3>Are You Ready for the Talent Consultant ?</h3>
                        <p>How workforce development programs turn talent puddles into talent pools , Hiring isn’t easy.
                            Employers might get hundreds </p>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <a href="#" class="theme-button-one float-right">Try It Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="views-element-container our-service section-margin-bottom-two"
             id="block-views-block-inobi-section-content-block-services">
            <div>
                <div class="container">
                    <div class="theme-title">
                        <h2>Business Services</h2>
                        <p>How workforce development programs turn talent puddles into talent pools , Hiring isn’t easy.
                            Employers <br> might get hundreds </p>
                        <a href="services-0.html" class="theme-button-one">See More Services</a>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-service">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/services/78914.jpg?itok=j5E5VTZo"
                                        alt="Web Development"></div>
                                <div class="text">
                                    <h4><a href="web-development.html">Web Development</a> <i
                                            class="flaticon-devices"></i></h4>
                                    <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita
                                        fusce temp oribus.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-service">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/services/107941.jpg?itok=ptdt9GE-"
                                        alt="Business Consulting"></div>
                                <div class="text">
                                    <h4><a href="business-consulting.html">Business Consulting</a> <i
                                            class="flaticon-upload"></i></h4>
                                    <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita
                                        fusce temp oribus.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-service">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/services/119b4b.jpg?itok=ktay7RgL"
                                        alt="Expert Team Mates"></div>
                                <div class="text">
                                    <h4><a href="expert-team-mates.html">Expert Team Mates</a> <i
                                            class="flaticon-devices"></i></h4>
                                    <p>Interdum iusto pulvinar consequuntur augue optio repellat fuga Purus expedita
                                        fusce temp oribus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block-weconsultiblockcontacthome" class="consultation-help clearfix section-margin-bottom">
            <div class="container">
                <div class="theme-title">
                    <h2>Request a Free Consultation</h2>
                    <p>
                        How workforce development programs turn talent puddles into talent pools , Hiring isn’t easy.
                        Employers might get hundreds
                    </p>
                </div>
            </div>
            <div class="clearfix">
                <div class="section-part float-left">
                    <div class="consultation-form-wrapper float-right col">
                        <form class="contact-message-consultation-help-from-form contact-message-form contact-form"
                              data-user-info-from-browser
                              data-drupal-selector="contact-message-consultation-help-from-form"
                              action="http://inobiz.drupalet.com/home1" method="post"
                              id="contact-message-consultation-help-from-form" accept-charset="UTF-8">
                            <div class="field--type-list-string field--name-field-investment-plan field--widget-options-select js-form-wrapper form-wrapper"
                                 data-drupal-selector="edit-field-investment-plan-wrapper"
                                 id="edit-field-investment-plan-wrapper">
                                <div class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-field-investment-plan form-item-field-investment-plan">
                                    <label for="edit-field-investment-plan">Investment Plan</label>
                                    <select data-drupal-selector="edit-field-investment-plan"
                                            id="edit-field-investment-plan" name="field_investment_plan"
                                            class="form-select">
                                        <option value="_none">- None -</option>
                                        <option value="1">Investment Plan</option>
                                        <option value="2">Material</option>
                                        <option value="3">Electronics</option>
                                        <option value="4">Industry</option>
                                    </select>
                                </div>
                            </div>
                            <input autocomplete="off"
                                   data-drupal-selector="form-yqg1suqwoxpyft3zlpm0a0x2c1c7y70o6xxkfpsqx9s" type="hidden"
                                   name="form_build_id" value="form-yQG1suqWOxPyFt3Zlpm0A0x2C1c7y70o6xxkFpsqX9s"/>
                            <input data-drupal-selector="edit-contact-message-consultation-help-from-form" type="hidden"
                                   name="form_id" value="contact_message_consultation_help_from_form"/>
                            <div class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-name form-item-name">
                                <label for="edit-name" class="js-form-required form-required">Your name</label>
                                <input data-drupal-selector="edit-name" type="text" id="edit-name" name="name" value=""
                                       size="60" maxlength="255" class="form-text required" required="required"
                                       aria-required="true"/>
                            </div>
                            <div class="field--type-language field--name-langcode field--widget-language-select js-form-wrapper form-wrapper"
                                 data-drupal-selector="edit-langcode-wrapper" id="edit-langcode-wrapper">
                            </div>
                            <div class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-mail form-item-mail">
                                <label for="edit-mail" class="js-form-required form-required">Your email address</label>
                                <input data-drupal-selector="edit-mail" type="email" id="edit-mail" name="mail" value=""
                                       size="60" maxlength="254" class="form-email required" required="required"
                                       aria-required="true"/>
                            </div>
                            <div class="field--type-telephone field--name-field-phone field--widget-telephone-default js-form-wrapper form-wrapper"
                                 data-drupal-selector="edit-field-phone-wrapper" id="edit-field-phone-wrapper">
                                <div class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-field-phone-0-value form-item-field-phone-0-value">
                                    <label for="edit-field-phone-0-value">Phone</label>
                                    <input data-drupal-selector="edit-field-phone-0-value" type="tel"
                                           id="edit-field-phone-0-value" name="field_phone[0][value]" value="" size="30"
                                           maxlength="128" placeholder="" class="form-tel"/>
                                </div>
                            </div>
                            <div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper"
                                 id="edit-actions"><input data-drupal-selector="edit-submit" type="submit"
                                                          id="edit-submit" name="op" value="Submit Request"
                                                          class="button button--primary js-form-submit form-submit theme-button-one p-bg-color"/>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="section-part float-left text-wrapper">
                    <div class="opacity clearfix">
                        <div class="col float-left">
                            <h3>27 Years of <br>Experience in <span class="p-color">Finance</span> <br>&amp; Business
                            </h3>
                            <p>Hiring isn’t easy. Employers might get hundreds — sometimes thousands — of resumes for a
                                single open position and still, somehow, hire the wrong </p>
                            <a href="service.html" class="theme-button-one">See Services</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="views-element-container our-project section-margin-bottom-two"
             id="block-views-block-inobi-section-content-block-projects">
            <div>
                <div class="container">
                    <div class="theme-title">
                        <h2>Our Projects</h2>
                        <p>How workforce development programs turn talent puddles into talent pools , Hiring isn’t easy.
                            Employers <br> might get hundreds </p>
                        <a href="project.html" class="theme-button-one">See More Projects</a>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-project">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x330/public/portfolio/91b51.jpg?itok=nz0wFMj7"
                                        alt="Io Business Inc."></div>
                                <div class="text">
                                    <h4><a href="io-business-inc.html">Io Business Inc.</a> <i
                                            class="flaticon-pie-chart"></i></h4>
                                    <p>Accounting</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-project">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x330/public/portfolio/102100.jpg?itok=BQlfW5pv"
                                        alt="Chanze Renovation Inc."></div>
                                <div class="text">
                                    <h4><a href="chanze-renovation-inc.html">Chanze Renovation Inc.</a> <i
                                            class="flaticon-pie-chart"></i></h4>
                                    <p>Idea</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-project">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x330/public/portfolio/2790e.jpg?itok=pjw-aq_G"
                                        alt="Cllo Financial Inc."></div>
                                <div class="text">
                                    <h4><a href="cllo-financial-inc.html">Cllo Financial Inc.</a> <i
                                            class="flaticon-puzzle"></i></h4>
                                    <p>Accounting</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="views-element-container client-section section-margin-bottom"
             id="block-views-block-inobi-section-content-block-testimonial-logo">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-xs-12">
                            <div class="client-slider-content">
                                <h3>Client Say</h3>
                                <div class="client-slider owl-carousel sliderImgs owl-loaded">
                                    <div class="item">
                                        <div class="wrapper">
                                            <div class="name">
                                                <p>Mahfuz Riad <span>( CEO )</span></p>
                                                <div class="icon"><img
                                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image28x36/public/2018-05/4_0d99d.jpg?itok=hGfjN7k-"
                                                        alt="Mahfuz Riad "></div>
                                            </div>
                                            <div class="text">
                                                <h5>Great Service Inobi </h5>
                                                <p>Incredibly easy to use them and extremely versatile! This is one of
                                                    the best quality themes I have purchased. I’m impressed with the
                                                    features. Of note, customer service was very friendly,
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="wrapper">
                                            <div class="name">
                                                <p>Rainer Maria <span>( CEO )</span></p>
                                                <div class="icon"><img
                                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image28x36/public/2018-05/5_0d931.jpg?itok=RvOLkMj2"
                                                        alt="Rainer Maria"></div>
                                            </div>
                                            <div class="text">
                                                <h5>Great Service Inobi </h5>
                                                <p>Incredibly easy to use them and extremely versatile! This is one of
                                                    the best quality themes I have purchased. I’m impressed with the
                                                    features. Of note, customer service was very friendly,
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="views-element-container">
                            <div class="col-md-5 col-xs-12">
                                <div class="our-partner">
                                    <h3>Our partner</h3>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-3 col-xs-6"><img
                                                src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image200x112/public/logo/p45359.jpg?itok=Rid2Kw5X"
                                                alt="Image alt"></div>
                                        <div class="col-md-6 col-sm-3 col-xs-6"><img
                                                src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image200x112/public/logo/p17ba5.jpg?itok=EunJZnqc"
                                                alt="Image alt"></div>
                                        <div class="col-md-6 col-sm-3 col-xs-6"><img
                                                src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image200x112/public/logo/p279f1.jpg?itok=bbS-jro8"
                                                alt="Image alt"></div>
                                        <div class="col-md-6 col-sm-3 col-xs-6"><img
                                                src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image200x112/public/logo/p304dc.jpg?itok=pESaPtwm"
                                                alt="Image alt"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="views-element-container our-blog section-margin-bottom-two"
             id="block-views-block-inobi-section-content-block-blog">
            <div>
                <div class="container">
                    <div class="theme-title">
                        <h2>Our News</h2>
                        <p>Our newst facts from our media office, programs turn talent puddles into talent pools ,
                            Hiring isn’t <br> easy. Employers might get hundreds </p>
                        <a href="blog.html" class="theme-button-one">See More News</a>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/blog/19ab7.jpg?itok=8U3WoRYZ"
                                        alt="Mauris volutpat aliquam tellus"></div>
                                <div class="text">
                                    <div class="date">22 May 2018</div>
                                    <h4><a href="mauris-volutpat-aliquam-tellus.html">Mauris volutpat aliquam tellus</a>
                                    </h4>
                                    By Mahfuz Riad
                                    <a href="mauris-volutpat-aliquam-tellus.html" class="view-more"><i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/blog/9c525.jpg?itok=twQ8JjZj"
                                        alt="Vestibulum convallis eros et"></div>
                                <div class="text">
                                    <div class="date">22 May 2018</div>
                                    <h4><a href="vestibulum-convallis-eros-et.html">Vestibulum convallis eros et</a>
                                    </h4>
                                    By Mahfuz Riad
                                    <a href="vestibulum-convallis-eros-et.html" class="view-more"><i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-blog">
                                <div class="image"><img
                                        src="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/styles/image370x207/public/blog/8928b.jpg?itok=pM2UN7Y3"
                                        alt="Pellentesque vitae aliquam mauris"></div>
                                <div class="text">
                                    <div class="date">22 May 2018</div>
                                    <h4><a href="pellentesque-vitae-aliquam-mauris.html">Pellentesque vitae aliquam
                                        mauris</a></h4>
                                    By Mahfuz Riad
                                    <a href="pellentesque-vitae-aliquam-mauris.html" class="view-more"><i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="block-inobibottombannerblock" class="bottom-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <h3>If you have any querry for related Business… Just touch on this button</h3>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <a href="contact.html" class="theme-button-one float-right">Contact us</a>
                    </div>
                </div>
            </div>
        </div>

        <footer class="theme-footer">
            <div class="container">
                <div class="top-footer row">
                    <div id="block-inobifooterlogoblock" class="col-md-4 col-sm-6 col-xs-12 footer-logo">
                        <a href="#"><img
                                src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/images/logo/logo.png"
                                alt="Logo"></a>
                        <p>We create business coaches and trainers, individual therapists, diet and nutrition
                            consultants, as well as other specialists who need a personal website, Check our latest work
                            in profile . . . .</p>
                        <h5>Follow us</h5>
                        <ul>
                            <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="tran3s"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div id="block-footer" class="col-md-2 col-sm-3 col-xs-12 footer-list">
                        <h5>About</h5>
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">About us</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Business
                                    Partners</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Faq</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Inobi</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Blog</a>
                            </li>
                            <li class="menu-item">
                                <a href="contact.html" data-drupal-link-system-path="contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <div id="block-menufooter2" class="col-md-2 col-sm-3 col-xs-12 footer-list">
                        <h5>Short link</h5>
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Business
                                    Growth</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Case
                                    Study</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Privacy</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Project</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Services</a>
                            </li>
                        </ul>
                    </div>
                    <div class="simplenews-subscriptions-block-a04558d6-ecd0-4845-bd21-cedaaf8be4ef simplenews-subscriber-form col-md-4 col-xs-12 Subscribe"
                         data-drupal-selector="simplenews-subscriptions-block-a04558d6-ecd0-4845-bd21-cedaaf8be4ef"
                         id="block-simplenewssubscription">
                        <h5>Subscribes</h5>
                        <form action="http://inobiz.drupalet.com/home1" method="post"
                              id="simplenews-subscriptions-block-a04558d6-ecd0-4845-bd21-cedaaf8be4ef"
                              accept-charset="UTF-8">
                            <div class="field--type-simplenews-subscription field--name-subscriptions field--widget-simplenews-subscription-select js-form-wrapper form-wrapper"
                                 data-drupal-selector="edit-subscriptions-wrapper"
                                 id="edit-subscriptions-wrapper"></div>
                            <div id="edit-message"
                                 class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-message form-item-message form-no-label">
                                Sign up for our mailing list to get latest updates and offers
                            </div>
                            <input autocomplete="off"
                                   data-drupal-selector="form-vzunui6ito7xhttftpf72ow-iitxkmzjkct8b4ijac" type="hidden"
                                   name="form_build_id" value="form-_vZUNUI6ITo7xhTtfTpf72OW_iItXKMZJKCt8b4Ijac"/>
                            <input data-drupal-selector="edit-simplenews-subscriptions-block-a04558d6-ecd0-4845-bd21-cedaaf8be4ef"
                                   type="hidden" name="form_id"
                                   value="simplenews_subscriptions_block_a04558d6-ecd0-4845-bd21-cedaaf8be4ef"/>
                            <div class="field--type-email field--name-mail field--widget-email-default js-form-wrapper form-wrapper"
                                 style="height: 75px; !important;" data-drupal-selector="edit-mail-wrapper"
                                 id="edit-mail-wrapper">
                                <div class="js-form-item form-item js-form-type-homepage form-type-homepage js-form-item-mail-0-value form-item-mail-0-value form-no-label">
                                    <input class="form-control form-opacity no-margin newsletter_input form-email required"
                                           data-drupal-selector="edit-mail-0-value" type="email" id="edit-mail-0-value"
                                           name="mail[0][value]" value="" size="60" maxlength="254"
                                           placeholder="Enter Your Email" required="required" aria-required="true"/>
                                </div>
                            </div>
                            <div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper"
                                 id="edit-actions--2"><input data-drupal-selector="edit-subscribe" type="submit"
                                                             id="edit-subscribe" name="op" value="Subscribe"
                                                             class="button js-form-submit form-submit theme-button-one p-bg-color"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="bottom-footer">
                <div class="container">
                    <p>Copyright &copy; 2018 <a href="#" class="tran3s">Inobi </a> Template by <a href="#"
                                                                                                  class="tran3s"
                                                                                                  target="_blank">Company
                        Name</a></p>
                </div>
            </div>
        </footer>
    </div>

    <button class="scroll-top tran3s">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </button>
</div>

<script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/domready/ready.min7016.js?v=1.0.8"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery/jquery.minf77b.js?v=3.2.1"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery-once/jquery.once.mind1f1.js?v=2.2.0"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupalSettingsLoader1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupal1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupal.init1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/bootstrap/bootstrap.min1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/bootstrap-select/dist/js/bootstrap-select1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/WOW-master/dist/wow.min1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/owl-carousel/owl.carousel.min1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/owl-carousel/owl.carousel1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/theme1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/map-script1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/update1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/debounce1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery.cookie/jquery.cookie.min290d.js?v=1.4.1"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/form1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/core/modules/statistics/statistics1cf9.js?v=8.5.3"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjQLCCbRKFhsr8BY78g2PQ0_bTyrm_YXU"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/nvs_geolocation/js/custom1cf9.js?v=8.5.3"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery.touchwipe2d2c.js?pa38zd"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/modernizr2d2c.js?pa38zd"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery.easing2d2c.js?pa38zd"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery-migrate-1.2.1.min2d2c.js?pa38zd"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/frontend/md-slider2d2c.js?pa38zd"></script>
<script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/frontend/init-md-slider2d2c.js?pa38zd"></script>

</body>
</html>

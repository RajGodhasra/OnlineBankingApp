<%--
  Created by IntelliJ IDEA.
  User: Raj Godhasara
  Date: 21-Jun-18
  Time: 4:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login | Online Banking</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" media="all"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/ajax-progress.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/autocomplete-loading.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/fieldgroup.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/container-inline.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/clearfix.module2d2c.css?pa3"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/details.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/hidden.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/item-list.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/js.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/nowrap.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/position-container.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/reset-appearance.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/resize.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/sticky-header.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-counter2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-report-counters2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/system-status-report-general-info2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tabledrag.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tablesort.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/stable/css/system/components/tree-child.module2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/animate2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/md-slider2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/css/md-slider-style2d2c.css?pa38zd"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/sites/default/files/md-slider-css/md-slider-home_slider_1-layers2d2c.css?pa38zd"/>

    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/action-links2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/breadcrumb2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/button2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/collapse-processed2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/container-inline2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/details2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/exposed-filters2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/field2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/form2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/icons2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/inline-form2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/item-list2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/link2d2c.css?pa38zd   "/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/links2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/more-link2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/pager2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tabledrag2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tableselect2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tablesort2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/tabs2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/textarea2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/ui-dialog2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/core/themes/classy/css/components/messages2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/style2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/responsive2d2c.css?pa38zd"/>
    <link rel="stylesheet"
          href="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/css/update2d2c.css?pa38zd"/>

    <%--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet"
          id="bootstrap-css">--%>

    <style>
        select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            height: 34px !important;
            width: 30% !important;
            background-color: white !important;
        }
    </style>
</head>
<body class="ltr path-frontpage home page-node-type-homepage home">

<a href="#main-content" class="visually-hidden focusable">
    Skip to main content
</a>
<div class="main-page-wrapper">
    <div id="inobi-map" class="hidden" data-lat="40.72" data-lang="-74"></div>
    <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
        <header class="theme-menu-wrapper">
            <div class="container">
                <div class="top-header ">
                    <div class="row">
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="call"><a href="#"><i class="flaticon-telephone"></i> +91 8866142423</a></div>
                        </div>
                        <div class="col-md-10 col-sm-3 col-xs-12">
                            <ul class="social-icon text-right">
                                <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                </li>
                                <li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header-wrapper clearfix">
                    <div id="block-inobi-branding" class="logo float-left">
                        <a href="<%=request.getContextPath() %>/index"><img
                                src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/logo.png"
                                alt="Home"></a>
                    </div>

                    <nav class="theme-main-menu navbar float-right" id="mega-menu-wrapper">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="navbar-collapse-1" class="collapse navbar-collapse">
                            <ul class="nav">
                                <li class="dropdown-holder">
                                    <a href="<%=request.getContextPath() %>/index" class="tran3s"
                                       data-drupal-link-system-path="&lt;front&gt;">Home</a>
                                    <%--<ul class="sub-menu">
                                        <li class="active">
                                            <a href="home1.html" class="tran3s is-active" data-drupal-link-system-path="node/37">Home version one</a>
                                        </li>
                                        <li>
                                            <a href="home2.html" class="tran3s" data-drupal-link-system-path="node/38">Home version two</a>
                                        </li>
                                    </ul>--%>
                                </li>
                                <%--<li class="dropdown-holder">
                                    <a href="#" class="tran3s is-active" data-drupal-link-system-path="&lt;front&gt;">Services</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="services-0.html" class="tran3s" data-drupal-link-system-path="node/40">Service</a>
                                        </li>
                                        <li>
                                            <a href="consulting-idea.html" class="tran3s" data-drupal-link-system-path="node/33">Services details</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder">
                                    <a href="#" class="tran3s is-active" data-drupal-link-system-path="&lt;front&gt;">Pages</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="404-page.html" class="tran3s" data-drupal-link-system-path="node/39">404 Page</a>
                                        </li>
                                        <li>
                                            <a href="about-us.html" class="tran3s" data-drupal-link-system-path="node/42">About us</a>
                                        </li>
                                        <li>
                                            <a href="node/46.html" class="tran3s" data-drupal-link-system-path="node/46">Faq</a>
                                        </li>
                                        <li>
                                            <a href="our-team.html" class="tran3s" data-drupal-link-system-path="node/43">Our Team</a>
                                        </li>
                                        <li class="dropdown-holder">
                                            <a href="shop.html" class="tran3s" data-drupal-link-system-path="shop">Shop</a>
                                            <ul class="second-sub-menu">
                                                <li>
                                                    <a href="long-tail-seo-strategy.html" class="tran3s" data-drupal-link-system-path="product/2">Shop Details</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder">
                                    <a href="#" class="tran3s" data-drupal-link-system-path="node/41">Porfolio</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="project.html" class="tran3s" data-drupal-link-system-path="node/41">Project</a>
                                        </li>
                                        <li>
                                            <a href="chanze-renovation-inc.html" class="tran3s" data-drupal-link-system-path="node/13">Project detail</a>
                                        </li>
                                    </ul>
                                </li>--%>
                                <li class="dropdown-holder active">
                                    <a href="<%=request.getContextPath() %>/login" class="tran3s  is-active"
                                       data-drupal-link-system-path="&lt;front&gt;">Login</a>
                                </li>
                                <li>
                                    <a href="#" class="tran3s" data-drupal-link-system-path="contact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>


        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->

        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="" id="loginModal">
                        <div class="modal-header">
                            <h3>Have an Account?</h3>
                        </div>
                        <div class="modal-body" style="max-height: 1000px; !important;">
                            <div class="well">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#login" data-toggle="tab">Login</a></li>
                                    <li><a href="#create" data-toggle="tab">Create Account</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane active in" id="login">
                                        <form class="form-horizontal" action='' method="POST">
                                            <fieldset style="margin-top: 20px;">
                                                <div id="legend">
                                                    <legend class="">Login</legend>
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="exampleInputEmail1" class="float-left"
                                                           style="margin-right: 25px; margin-top: 5px;">Email
                                                        address</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           aria-describedby="emailHelp" placeholder="Enter email">
                                                    <small id="emailHelp" class="form-text text-muted">We'll never share
                                                        your email with anyone else.
                                                    </small>
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="exampleInputPassword1" class="float-left"
                                                           style="margin-right: 55px; margin-top: 5px;">Password</label>
                                                    <input type="password" class="form-control"
                                                           id="exampleInputPassword1" placeholder="Password">
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <input type="checkbox" class="form-check-input" id="verifyUser">
                                                    <label class="form-check-label" for="verifyUser">Check me
                                                        out</label>
                                                    <label class="form-check-label"
                                                           style="color: blue; margin-left: 110px;"><a href="#">Forgot
                                                        Password?</a></label>
                                                </div>
                                                <input data-drupal-selector="edit-submit" type="submit" id="loginButton"
                                                       name="loginButton" value="Login"
                                                       class="button button--primary js-form-submit form-submit theme-button-one p-bg-color"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="create">
                                        <form id="tab" class="form-horizontal">
                                            <fieldset style="margin-top: 20px;">
                                                <div id="legend1">
                                                    <legend class="">Register</legend>
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="registerUsername" class="float-left"
                                                           style="margin-right: 50px; margin-top: 5px;">Username</label>
                                                    <input type="text" class="form-control" id="registerUsername"
                                                           placeholder="Enter Username" name="registerUsername">
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="registerFirstname" class="float-left"
                                                           style="margin-right: 46px; margin-top: 5px;">First
                                                        Name</label>
                                                    <input type="text" class="form-control" id="registerFirstname"
                                                           placeholder="Enter Firstname" name="registerFirstname">
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="registerLastname" class="float-left"
                                                           style="margin-right: 48px; margin-top: 5px;">Last
                                                        Name</label>
                                                    <input type="text" class="form-control" id="registerLastname"
                                                           placeholder="Enter Lastname" name="registerLastname">
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="registerEmail" class="float-left"
                                                           style="margin-right: 23px; margin-top: 5px;">Email
                                                        address</label>
                                                    <input type="email" class="form-control" id="registerEmail"
                                                           aria-describedby="registerEmailHelp"
                                                           placeholder="Enter Email">
                                                    <small id="registerEmailHelp" class="form-text text-muted">We'll
                                                        never share your email with anyone else.
                                                    </small>
                                                </div>
                                                <div style="padding-bottom: 10px;">
                                                    <label for="registerPassword" class="float-left"
                                                           style="margin-right: 52px; margin-top: 5px;">Password</label>
                                                    <input type="text" class="form-control" id="registerPassword"
                                                           placeholder="Enter Password" name="registerPassword">
                                                </div>
                                                <input data-drupal-selector="edit-submit" type="submit"
                                                       id="registerButton" name="registerButton" value="Register"
                                                       class="button button--primary js-form-submit form-submit theme-button-one p-bg-color"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/domready/ready.min7016.js?v=1.0.8"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery/jquery.minf77b.js?v=3.2.1"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery-once/jquery.once.mind1f1.js?v=2.2.0"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupalSettingsLoader1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupal1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/drupal.init1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/bootstrap/bootstrap.min1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/bootstrap-select/dist/js/bootstrap-select1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/WOW-master/dist/wow.min1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/owl-carousel/owl.carousel.min1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/vendor/owl-carousel/owl.carousel1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/theme1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/map-script1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/themes/inobi/js/update1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/debounce1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/assets/vendor/jquery.cookie/jquery.cookie.min290d.js?v=1.4.1"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/misc/form1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/core/modules/statistics/statistics1cf9.js?v=8.5.3"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjQLCCbRKFhsr8BY78g2PQ0_bTyrm_YXU"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/nvs_geolocation/js/custom1cf9.js?v=8.5.3"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery.touchwipe2d2c.js?pa38zd"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/modernizr2d2c.js?pa38zd"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery.easing2d2c.js?pa38zd"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/libraries/jquery-migrate-1.2.1.min2d2c.js?pa38zd"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/frontend/md-slider2d2c.js?pa38zd"></script>
    <script src="<%=request.getContextPath() %>/resources/maintemp/modules/md_slider-8.x-1.0/md_slider/assets/js/frontend/init-md-slider2d2c.js?pa38zd"></script>

</div>
</body>
</html>

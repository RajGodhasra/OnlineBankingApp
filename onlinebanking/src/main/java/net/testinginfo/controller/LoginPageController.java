package net.testinginfo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginPageController {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String get(ModelMap model, HttpServletRequest request){
        return "login";
    }
}

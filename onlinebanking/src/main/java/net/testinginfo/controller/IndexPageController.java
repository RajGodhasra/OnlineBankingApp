package net.testinginfo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexPageController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getDefault(ModelMap model, HttpServletRequest request){
        return "index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String get(ModelMap model, HttpServletRequest request){
        return "index";
    }
}
